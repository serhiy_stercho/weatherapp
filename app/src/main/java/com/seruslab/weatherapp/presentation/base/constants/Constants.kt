package com.seruslab.weatherapp.presentation.base.constants

object Constants {
    const val WEATHER_IMAGEBASE_URL = "http://openweathermap.org/img/wn/"
    const val PNG_EXT = ".png"
}