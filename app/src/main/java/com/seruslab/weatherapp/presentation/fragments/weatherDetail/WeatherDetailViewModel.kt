package com.seruslab.weatherapp.presentation.fragments.weatherDetail

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.seruslab.weatherapp.domain.model.BaseWeatherListModel
import com.seruslab.weatherapp.domain.model.toCurrentCityWeatherModel
import com.seruslab.weatherapp.domain.useCases.IGetLocalWeatherDataUseCase
import com.seruslab.weatherapp.domain.useCases.IGetWeatherDataFromNetworkUseCase
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.lang.Exception

class WeatherDetailViewModel(private val iGetLocalWeatherDataUseCase: IGetLocalWeatherDataUseCase,
                             private val iGetWeatherDataFromNetwork: IGetWeatherDataFromNetworkUseCase) : ViewModel() {

    val weather = MutableLiveData<BaseWeatherListModel>()
    val errors = MutableLiveData<Throwable>()
    val compositeDisposable = CompositeDisposable()

    init {

    }

    fun loadWeatherDataByCityName(name: String) {

        iGetLocalWeatherDataUseCase.getWeatherByCityName(name)
            .flatMapObservable {localWeatherModel ->
                iGetWeatherDataFromNetwork.getWeatherByName(name)
                    .toObservable()
                    .startWith(localWeatherModel)

            }
            .onErrorResumeNext(Observable.empty())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({_weatherModel ->
                if (!_weatherModel.isEmpty()) {
                    weather.value = _weatherModel.toCurrentCityWeatherModel()
                }

            },{
                errors.value = it
            })
            .let {
                compositeDisposable.add(it)
            }

    }

}