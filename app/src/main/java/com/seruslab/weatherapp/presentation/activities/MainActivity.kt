package com.seruslab.weatherapp.presentation.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.seruslab.weatherapp.R
import com.seruslab.weatherapp.databinding.ActivityMainBinding
import com.seruslab.weatherapp.presentation.managers.CurrentLocationManager
import org.koin.core.KoinComponent
import org.koin.core.inject

class MainActivity : AppCompatActivity(), KoinComponent {

    val locationManager: CurrentLocationManager by inject()

    private lateinit var navController: NavController
    private lateinit var mBinbing: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        locationManager.registerActivity(this)
        setupLocationManager()
        mBinbing = DataBindingUtil.setContentView(this, R.layout.activity_main)
        navController = findNavController(R.id.fragmentMainContainer_MA)
    }

    private fun setupLocationManager() {
        lifecycle.addObserver(locationManager)
    }

    override fun onSupportNavigateUp() = navController.navigateUp()

    override fun onRestart() {
        super.onRestart()
        locationManager.registerActivity(this)
    }

    override fun onStop() {
        locationManager.unregisterActivity(this)
        super.onStop()
    }

    override fun onDestroy() {
        lifecycle.removeObserver(locationManager)
        super.onDestroy()
    }
}