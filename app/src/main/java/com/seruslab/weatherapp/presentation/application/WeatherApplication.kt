package com.seruslab.weatherapp.presentation.application

import android.app.Application
import com.facebook.stetho.Stetho

import com.seruslab.weatherapp.data.api.applicationApi.client.WeatherAppRestClient
import com.seruslab.weatherapp.data.api.locationServicesApi.client.LocationServicesRestClient
import com.seruslab.weatherapp.data.persistence.database.WeatherDatabase
import com.seruslab.weatherapp.di.*
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin

class WeatherApplication : Application(), KoinComponent {

    val weatherRestClient: WeatherAppRestClient by inject()
    val locationServicesRestClient: LocationServicesRestClient by inject()
    val weatherDatabase: WeatherDatabase by inject()

    override fun onCreate() {
        super.onCreate()
        startupKoin()
        instance = this
        Stetho.initializeWithDefaults(this)
    }


    fun startupKoin() {
        startKoin {
            androidContext(this@WeatherApplication)
            modules(listOf(
                appModule,
                roomModule,
                repositoriesModule,
                useCasesModule,
                viewModuleModule,
                homeFragmentModule
            ))
        }
    }

    companion object {
        private lateinit var instance: WeatherApplication

        fun getInstance() = instance
    }
}