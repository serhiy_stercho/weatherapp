package com.seruslab.weatherapp.presentation.fragments.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.seruslab.weatherapp.data.response.city.CityResponse
import com.seruslab.weatherapp.data.response.weather.Coordinates
import com.seruslab.weatherapp.domain.model.BaseWeatherListModel
import com.seruslab.weatherapp.domain.model.toCurrentCityWeatherModel
import com.seruslab.weatherapp.domain.model.toOtherCityWeatherModel
import com.seruslab.weatherapp.domain.useCases.IGetLocalWeatherDataUseCase
import com.seruslab.weatherapp.domain.useCases.IGetWeatherDataFromNetworkUseCase
import com.seruslab.weatherapp.domain.useCases.ISearchCitiesUseCase
import com.seruslab.weatherapp.presentation.fragments.home.adapter.model.CurrentCityWeatherModel
import com.seruslab.weatherapp.presentation.managers.CurrentLocationManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class HomeViewModel(private val iSearchCitiesUseCase: ISearchCitiesUseCase,
                    private val iGetWeatherDataFromNetworkUseCase: IGetWeatherDataFromNetworkUseCase,
                    private val iGetLocalWeatherDataUseCase: IGetLocalWeatherDataUseCase) : ViewModel() {

    val foundCities = MutableLiveData<List<CityResponse>>()
    val currentPlaceWeather = MutableLiveData<CurrentCityWeatherModel>()
    val savedCitiesWeather = MutableLiveData<List<BaseWeatherListModel>>()
    val compositeDisposable = CompositeDisposable()

    init {
        //getSavedLocationsWeather()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun getSavedLocationsWeather() {
        //iGetWeatherDataFromNetworkUseCase
        iGetLocalWeatherDataUseCase.getAllCitiesWeather()
            .map {
                it.map {
                    if(it.isCurrentPlace) it.toCurrentCityWeatherModel() else  it.toOtherCityWeatherModel()
                }.sortedByDescending { it.isCurrentPlace }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({_weather ->
                savedCitiesWeather.value = _weather
            }, {
                Log.i("weather", it.localizedMessage)
            })
            .let { compositeDisposable.add(it) }
    }


    fun findWeatherByCurrentCoordinates(coord: Coordinates) {
        iGetWeatherDataFromNetworkUseCase.getWeatherByCoordinates(coord)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({_weather ->
                currentPlaceWeather.value = _weather.toCurrentCityWeatherModel()
            }, {
                Log.i("weather", it.localizedMessage)
            })
            .let { compositeDisposable.add(it) }

    }

    fun setupQuery(textQuery: String) {
        iSearchCitiesUseCase.execute(textQuery)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({_cities ->
                foundCities.value = _cities
            }, {

            })
            .let { compositeDisposable.add(it) }
    }
}