package com.seruslab.weatherapp.presentation.managers

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.Relay
import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.single.BasePermissionListener
import com.karumi.dexter.listener.single.CompositePermissionListener
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener
import com.seruslab.weatherapp.R
import com.seruslab.weatherapp.data.response.weather.Coordinates

class CurrentLocationManager(private val mContext: Context) : LifecycleObserver {

    private val mFusedLocationClient: FusedLocationProviderClient
    private val mActivitiesList: MutableSet<AppCompatActivity>
    private lateinit var mLocationRequest: LocationRequest

    val locationStream: Relay<Coordinates> = BehaviorRelay.create()

    init {
        mActivitiesList = linkedSetOf()
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext)
        createLocationRequest()
    }

    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    fun startLocationUpdate() {
        checkPermission()
    }

    private fun checkPermission() {

        val dialogPermissionListener = DialogOnDeniedPermissionListener.Builder
            .withContext(getActivity())
            .withTitle("Location Permission")
            .withMessage("Need location permission to find user location")
            .withButtonText(android.R.string.ok)
            .withIcon(R.mipmap.ic_launcher)
            .build()

        val compositePermissionListener = CompositePermissionListener(locationPermissionListener, dialogPermissionListener)

        Dexter.withActivity(getActivity())
            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(compositePermissionListener)
            .withErrorListener { error -> Log.e("Dexter", "There was an error: $error") }
            .check()
    }

    private val locationPermissionListener = object : BasePermissionListener() {
        override fun onPermissionGranted(response: PermissionGrantedResponse?) {
           start()
        }

        override fun onPermissionDenied(response: PermissionDeniedResponse?) {

        }
    }

    private fun start() {
        if (ActivityCompat.checkSelfPermission(
                mContext,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(
                mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        mFusedLocationClient.lastLocation.addOnSuccessListener {location ->
            location?.let {
                locationStream.accept(Coordinates(it.latitude, it.longitude))
            }
        }

        startLocationTracking()
    }

    private fun startLocationTracking() {
        checkLocationSettings()
    }

    private fun checkLocationSettings() {
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest)

        val client = LocationServices.getSettingsClient(mContext)
        val task = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener(settingsAreGoodCallback)
        task.addOnFailureListener(settingsAreBad)
    }

    private val settingsAreGoodCallback = object : OnSuccessListener<LocationSettingsResponse> {
        override fun onSuccess(p0: LocationSettingsResponse?) {
            requestLocationUpdate()
        }
    }

    private fun requestLocationUpdate() {
        if (ActivityCompat.checkSelfPermission(
                mContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(
                mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest,
            locationCallback,
            null /* Looper */
        )
    }

    private val settingsAreBad = OnFailureListener {e ->
        val statusCode = (e as ApiException).statusCode
        when (statusCode) {
            CommonStatusCodes.RESOLUTION_REQUIRED ->
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    val resolvable = e as ResolvableApiException
                    resolvable.startResolutionForResult(
                        getActivity(),
                        REQUEST_CHECK_SETTINGS
                    )
                } catch (sendEx: SendIntentException) {
                    // Ignore the error.
                }
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
            }
        }
    }

    private val locationCallback = object :  LocationCallback() {

        override fun onLocationResult(locationResult: LocationResult?) {
            locationResult?.let {
                it.lastLocation?.let {
                    locationStream.accept(Coordinates(it.latitude, it.longitude))
                }
            }
        }
    }

    fun onActivityResult(_requestCode: Int, _resultCode: Int) {
        if (_requestCode == REQUEST_CHECK_SETTINGS && _resultCode == Activity.RESULT_OK) {
            startLocationUpdate()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(locationCallback)
    }

    fun registerActivity(activity: AppCompatActivity) {
        mActivitiesList.add(activity)
    }

    fun unregisterActivity(activity: AppCompatActivity) {
        mActivitiesList.remove(activity)
    }

    private fun getActivity() = mActivitiesList.iterator().next()

    companion object {
        const val REQUEST_CHECK_SETTINGS = 1090
    }

}