package com.seruslab.weatherapp.presentation.fragments.weatherDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.seruslab.weatherapp.R
import com.seruslab.weatherapp.databinding.FragmentWeatherDetailBinding
import com.seruslab.weatherapp.presentation.base.constants.Constants
import com.seruslab.weatherapp.presentation.base.glide.GlideApp
import kotlinx.android.synthetic.main.toolbar.view.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.KoinComponent

class WeatherDetailFragment : Fragment(), KoinComponent {

    private lateinit var mBinding: FragmentWeatherDetailBinding
    private val mViewModel: WeatherDetailViewModel by viewModel()

    val arguments: WeatherDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_weather_detail,
            container,
            false
        )
        mBinding.lifecycleOwner = this
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        observeWeather()
        observeErrors()
        arguments.cityName?.let {
            mViewModel.loadWeatherDataByCityName(it)
        }
    }

    private fun setupToolbar(){
        (activity as AppCompatActivity).setSupportActionBar(mBinding.toolbarFWD as Toolbar)
        (activity as AppCompatActivity).supportActionBar?.apply {
            title = getString(R.string.weather_detail)
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    private fun observeWeather() {
        mViewModel.weather.observe(this, Observer {_weather ->
            mBinding.weather = _weather
            mBinding.executePendingBindings()
        })
    }

    private fun observeErrors() {
        mViewModel.errors.observe(this, Observer {
            Toast.makeText(context, it.localizedMessage, Toast.LENGTH_SHORT).show()
        })
    }

    companion object {

    }
}