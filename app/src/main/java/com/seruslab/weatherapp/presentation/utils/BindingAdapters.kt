package com.seruslab.weatherapp.presentation.utils

import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.seruslab.weatherapp.R
import com.seruslab.weatherapp.presentation.base.glide.GlideApp

object BindingAdapters {

    @JvmStatic
    @BindingAdapter("app:image")
    fun setImage(image: AppCompatImageView, url: String?) {
        GlideApp.with(image.context)
            .load(url)
            .error(R.drawable.ic_wb_cloudy)
            .into(image)
    }
}