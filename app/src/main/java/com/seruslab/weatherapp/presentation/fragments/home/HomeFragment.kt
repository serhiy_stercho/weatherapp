package com.seruslab.weatherapp.presentation.fragments.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding2.widget.RxTextView
import com.seruslab.weatherapp.R
import com.seruslab.weatherapp.data.response.city.CityResponse
import com.seruslab.weatherapp.databinding.FragmentHomeBinding
import com.seruslab.weatherapp.domain.model.BaseWeatherListModel
import com.seruslab.weatherapp.presentation.fragments.home.adapter.HomeWeatherListAdapter
import com.seruslab.weatherapp.presentation.managers.CurrentLocationManager
import io.reactivex.disposables.CompositeDisposable
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.concurrent.TimeUnit

class HomeFragment : Fragment() {

    lateinit var mBinding: FragmentHomeBinding

    private val mViewModel: HomeViewModel by viewModel()
    private val mLocationManager: CurrentLocationManager by inject()
    private val mHomeWeatherAdapter: HomeWeatherListAdapter by inject()

    val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(mLocationManager)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_home,
            container,
            false
        )
        return mBinding.root
    }

    override fun onStart() {
        super.onStart()
        mViewModel.getSavedLocationsWeather()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupStream()
        setupAdapter()
        observeFoundCities()
        observeCurrentPlaceWeather()
        observeSavedCitiesWeather()
        mLocationManager.startLocationUpdate()
    }


    private fun setupAdapter() {
        mBinding.rvPlacesFH.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = mHomeWeatherAdapter
        }
        mHomeWeatherAdapter.setupClickInteractor(object : HomeWeatherListAdapter.ClickInteractor {
            override fun onItemClick(model: BaseWeatherListModel) {
                findNavController().navigate(HomeFragmentDirections
                    .actionOpenWeatherDetailByName(model.cityName))
            }
        })
    }

    private fun setupStream() {
        mLocationManager.locationStream.take(1)
            .doOnNext { mViewModel.findWeatherByCurrentCoordinates(it) }
            .subscribe()
            .let {
                compositeDisposable.add(it)
            }

        RxTextView.textChangeEvents(mBinding.edtSearchPlaceFH)
            .share()
            .debounce(400, TimeUnit.MILLISECONDS)
            .filter {_event -> _event.text().toString().length > 2}
            .doOnNext {event ->  mViewModel.setupQuery(event.text().toString().trim())}
            .subscribe()
            .let {
                compositeDisposable.add(it)
            }
    }

    private fun observeFoundCities() {
        mViewModel.foundCities.observe(this, Observer {_cities ->
            _cities.let {
               val adapter = ArrayAdapter<String>(context!!,
                    android.R.layout.simple_dropdown_item_1line, _cities.map { it.description })
                mBinding.edtSearchPlaceFH.setAdapter(adapter)
                mBinding.edtSearchPlaceFH.setOnItemClickListener {parent, view, position, id ->
                    findNavController()
                        .navigate(HomeFragmentDirections
                            .actionOpenWeatherDetailByName(_cities.get(position).structuredFormatting.main))
                }
            }
        })
    }

    private fun observeCurrentPlaceWeather() {
        mViewModel.currentPlaceWeather.observe(this, Observer {_currentWeather ->
            mHomeWeatherAdapter.addAsFirstItem(_currentWeather)
        })
    }

    private fun observeSavedCitiesWeather() {
        mViewModel.savedCitiesWeather.observe(this, Observer {
            mHomeWeatherAdapter.setupItems(it)
        })
    }

    override fun onDestroyView() {
        lifecycle.removeObserver(mLocationManager)
        compositeDisposable.clear()
        super.onDestroyView()
    }
}