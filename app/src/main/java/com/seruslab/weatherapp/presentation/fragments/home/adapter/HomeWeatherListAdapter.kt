package com.seruslab.weatherapp.presentation.fragments.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.seruslab.weatherapp.R
import com.seruslab.weatherapp.databinding.ItemCityBinding
import com.seruslab.weatherapp.databinding.ItemCurrentPlaceForecastBinding
import com.seruslab.weatherapp.presentation.base.BaseViewHolder
import com.seruslab.weatherapp.domain.model.BaseWeatherListModel
import com.seruslab.weatherapp.presentation.base.constants.Constants
import com.seruslab.weatherapp.presentation.base.glide.GlideApp
import com.seruslab.weatherapp.presentation.fragments.home.adapter.model.CurrentCityWeatherModel
import com.seruslab.weatherapp.presentation.fragments.home.adapter.model.OtherCityWeatherModel
import java.lang.RuntimeException

class HomeWeatherListAdapter : ListAdapter<BaseWeatherListModel, BaseViewHolder>(diffCallback) {

    private lateinit var mClickInteractor: ClickInteractor
    private lateinit var mLifecycleOwner: LifecycleOwner
    private var itemsList: ArrayList<BaseWeatherListModel>

    init {
        itemsList = arrayListOf()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when(viewType) {
            R.layout.item_current_place_forecast -> {
                val binding = ItemCurrentPlaceForecastBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                CurrentCityWeatherViewHolder(binding)
            }
            R.layout.item_city -> OtherCitiesViewHolder(
                ItemCityBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> throw RuntimeException("Can't create view holder for viewtype")
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemViewType(position: Int): Int {
        val model = getItem(position)
        return when(model){
            is CurrentCityWeatherModel -> R.layout.item_current_place_forecast
            is OtherCityWeatherModel -> R.layout.item_city
            else -> throw RuntimeException("No item view type found")
        }
    }

    fun addAsFirstItem(item: BaseWeatherListModel) {
        if (itemsList.firstOrNull() == null){
            itemsList = arrayListOf(item)
            submitList(itemsList)
            return
        }

        if(itemsList.first() is CurrentCityWeatherModel) {
            itemsList.set(0, item)
            submitList(itemsList)
        } else {
            val newList = arrayListOf<BaseWeatherListModel>()
            newList.add(item)
            newList.addAll(itemsList)
            itemsList = newList
            submitList(itemsList)
        }
    }

    fun setupItems(_items: List<BaseWeatherListModel>) {
        itemsList = ArrayList(_items)
        submitList(_items)
    }

    fun addItems(_items: List<BaseWeatherListModel>) {
        val oldCount = itemCount
        val items = ArrayList(itemsList)
        items.addAll(_items)
        itemsList = ArrayList(_items)
        submitList(itemsList)
        notifyItemRangeInserted(oldCount, itemCount - 1)
    }

    fun setupClickInteractor(interactor: ClickInteractor) {
        mClickInteractor = interactor
    }


    inner class CurrentCityWeatherViewHolder(val binding: ItemCurrentPlaceForecastBinding) : BaseViewHolder(binding.root) {

        override fun bind() {
            val item = getItem(adapterPosition) as CurrentCityWeatherModel
            binding.weather = item
            binding.executePendingBindings()
        }

    }

    inner class OtherCitiesViewHolder(val binding: ItemCityBinding) : BaseViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                mClickInteractor.onItemClick(getItem(adapterPosition))
            }
        }

        override fun bind() {
            val item = getItem(adapterPosition) as OtherCityWeatherModel

            binding.weather = item
            binding.executePendingBindings()

        }
    }

    interface ClickInteractor {
        fun onItemClick(model: BaseWeatherListModel)
    }

    companion object {
        val diffCallback = object : DiffUtil.ItemCallback<BaseWeatherListModel>() {
            override fun areContentsTheSame(oldItem: BaseWeatherListModel, newItem: BaseWeatherListModel): Boolean {
                return oldItem.equals(newItem)
            }

            override fun areItemsTheSame(oldItem: BaseWeatherListModel, newItem: BaseWeatherListModel): Boolean {
                return oldItem.equals(newItem)
            }
        }
    }

}