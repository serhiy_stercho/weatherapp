package com.seruslab.weatherapp.presentation.fragments.home.adapter.model

import com.seruslab.weatherapp.domain.model.BaseWeatherListModel

class OtherCityWeatherModel(override var cityId: Int?,
                            override val cityName: String,
                            override val humidity: Int,
                            override val maxTemperature: Double,
                            override val minTemperature: Double,
                            override val main: String,
                            override val icon: String,
                            override val isCurrentPlace: Boolean
) : BaseWeatherListModel(cityId, cityName, humidity, maxTemperature, minTemperature, main, icon, isCurrentPlace) {
}