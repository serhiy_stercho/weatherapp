package com.seruslab.weatherapp.di

import com.seruslab.weatherapp.domain.useCases.IGetLocalWeatherDataUseCase
import com.seruslab.weatherapp.domain.useCases.IGetWeatherDataFromNetworkUseCase
import com.seruslab.weatherapp.domain.useCases.ISaveWeatherForCityUseCase
import com.seruslab.weatherapp.domain.useCases.ISearchCitiesUseCase
import com.seruslab.weatherapp.domain.useCases.implementation.GetLocalWeatherDataUseCase
import com.seruslab.weatherapp.domain.useCases.implementation.GetWeatherDataFromNetworkUseCase
import com.seruslab.weatherapp.domain.useCases.implementation.SaveWeatherForCityUseCase
import com.seruslab.weatherapp.domain.useCases.implementation.SearchCitiesUseCase
import org.koin.dsl.module

val useCasesModule = module {

    factory<ISearchCitiesUseCase> { SearchCitiesUseCase(get()) }
    factory<ISaveWeatherForCityUseCase> { SaveWeatherForCityUseCase(get()) }
    factory<IGetWeatherDataFromNetworkUseCase> { GetWeatherDataFromNetworkUseCase(get(), get(), get()) }
    factory<IGetLocalWeatherDataUseCase> { GetLocalWeatherDataUseCase(get()) }
}