package com.seruslab.weatherapp.di

import androidx.room.Room
import com.seruslab.weatherapp.data.persistence.database.WeatherDatabase
import org.koin.dsl.module

val roomModule = module {

    single {
        Room.databaseBuilder(get(), WeatherDatabase::class.java, "weather-database")
            .fallbackToDestructiveMigration()
            .build()
    }

}