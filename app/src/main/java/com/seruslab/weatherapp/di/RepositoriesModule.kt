package com.seruslab.weatherapp.di

import com.seruslab.weatherapp.data.repositories.IGetLocalWeatherDataRepository
import com.seruslab.weatherapp.data.repositories.IGetWeatherRepository
import com.seruslab.weatherapp.data.repositories.ISaveWeatherDataLocallyRepository
import com.seruslab.weatherapp.data.repositories.ISearchCitiesRepository
import com.seruslab.weatherapp.data.repositories.implementation.GetLocalWeatherDataRepository
import com.seruslab.weatherapp.data.repositories.implementation.GetWeatherRepository
import com.seruslab.weatherapp.data.repositories.implementation.SaveWeatherDataLocallyRepository
import com.seruslab.weatherapp.data.repositories.implementation.SearchCitiesRepository
import org.koin.dsl.module

val repositoriesModule = module {

    factory <IGetWeatherRepository> { GetWeatherRepository() }
    factory<ISaveWeatherDataLocallyRepository> { SaveWeatherDataLocallyRepository() }
    factory<ISearchCitiesRepository> { SearchCitiesRepository() }
    factory<IGetLocalWeatherDataRepository> { GetLocalWeatherDataRepository() }
}