package com.seruslab.weatherapp.di

import com.seruslab.weatherapp.presentation.fragments.home.HomeViewModel
import com.seruslab.weatherapp.presentation.fragments.weatherDetail.WeatherDetailViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModuleModule = module {

    viewModel { HomeViewModel(get(), get(), get()) }
    viewModel { WeatherDetailViewModel(get(), get()) }
}