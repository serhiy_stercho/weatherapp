package com.seruslab.weatherapp.di

import com.seruslab.weatherapp.data.api.applicationApi.client.QueryParameterInterceptor
import com.seruslab.weatherapp.data.api.applicationApi.client.WeatherAppRestClient
import com.seruslab.weatherapp.data.api.locationServicesApi.client.LocationQueryParameterInterceptor
import com.seruslab.weatherapp.data.api.locationServicesApi.client.LocationServicesRestClient
import com.seruslab.weatherapp.presentation.managers.CurrentLocationManager
import okhttp3.Interceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module

val appModule = module {

    single { WeatherAppRestClient() }
    single(named("appQueryInterceptor")) { QueryParameterInterceptor() }

    single { LocationServicesRestClient() }
    single(named("locationQueryInterceptor")) { LocationQueryParameterInterceptor() }

    single { CurrentLocationManager(get()) }
}