package com.seruslab.weatherapp.di

import com.seruslab.weatherapp.presentation.fragments.home.adapter.HomeWeatherListAdapter
import org.koin.dsl.module

val homeFragmentModule = module {

    factory { HomeWeatherListAdapter() }
}