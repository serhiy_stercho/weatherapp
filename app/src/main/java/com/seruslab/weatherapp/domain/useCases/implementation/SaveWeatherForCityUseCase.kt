package com.seruslab.weatherapp.domain.useCases.implementation

import com.seruslab.weatherapp.data.persistence.entities.WeatherEntity
import com.seruslab.weatherapp.data.repositories.ISaveWeatherDataLocallyRepository
import com.seruslab.weatherapp.domain.useCases.ISaveWeatherForCityUseCase
import io.reactivex.Completable

class SaveWeatherForCityUseCase(private val iSaveWeatherDataLocallyRepository: ISaveWeatherDataLocallyRepository)
    : ISaveWeatherForCityUseCase {

    override fun execute(entity: WeatherEntity): Completable =
        iSaveWeatherDataLocallyRepository.saveWeatherConditions(entity)

}