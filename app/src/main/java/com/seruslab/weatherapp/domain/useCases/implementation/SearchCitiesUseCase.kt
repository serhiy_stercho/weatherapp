package com.seruslab.weatherapp.domain.useCases.implementation

import com.seruslab.weatherapp.data.repositories.ISearchCitiesRepository
import com.seruslab.weatherapp.data.response.city.CityResponse
import com.seruslab.weatherapp.domain.useCases.ISearchCitiesUseCase
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class SearchCitiesUseCase(private val iSearchCitiesRepository: ISearchCitiesRepository) : ISearchCitiesUseCase {

    override fun execute(query: String): Observable<List<CityResponse>> =
        iSearchCitiesRepository.searchCityByQuery(query)
            .distinctUntilChanged()

}