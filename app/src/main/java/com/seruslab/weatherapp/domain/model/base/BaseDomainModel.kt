package com.seruslab.weatherapp.domain.model.base

data class BaseDomainModel<M>(val data: M?)