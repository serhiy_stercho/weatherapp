package com.seruslab.weatherapp.domain.useCases

import com.seruslab.weatherapp.data.persistence.entities.WeatherEntity
import io.reactivex.Completable

interface ISaveWeatherForCityUseCase {
    fun execute(entity: WeatherEntity) : Completable
}