package com.seruslab.weatherapp.domain.useCases

import com.seruslab.weatherapp.data.response.city.CityResponse
import io.reactivex.Observable


interface ISearchCitiesUseCase {
    fun execute(query: String) : Observable<List<CityResponse>>
}