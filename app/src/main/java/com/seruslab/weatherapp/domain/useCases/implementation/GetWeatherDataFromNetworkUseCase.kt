package com.seruslab.weatherapp.domain.useCases.implementation

import android.util.Log
import com.seruslab.weatherapp.data.repositories.IGetLocalWeatherDataRepository
import com.seruslab.weatherapp.data.repositories.IGetWeatherRepository
import com.seruslab.weatherapp.data.response.weather.Coordinates
import com.seruslab.weatherapp.data.response.weather.toWeatherEntity
import com.seruslab.weatherapp.data.response.weather.toWeatherModel
import com.seruslab.weatherapp.domain.model.BaseWeatherListModel
import com.seruslab.weatherapp.domain.useCases.IGetWeatherDataFromNetworkUseCase
import com.seruslab.weatherapp.domain.useCases.ISaveWeatherForCityUseCase
import io.reactivex.Single

class GetWeatherDataFromNetworkUseCase(private val iGetWeatherDataRepository: IGetWeatherRepository,
                                       private val iGetLocalWeatherDataRepository: IGetLocalWeatherDataRepository,
                                       private val iSaveWeatherForCityUseCase: ISaveWeatherForCityUseCase) : IGetWeatherDataFromNetworkUseCase {


    override fun getWeatherByName(name: String): Single<BaseWeatherListModel> {
        return iGetWeatherDataRepository.getByCityName(name)
            .flatMap {
                iSaveWeatherForCityUseCase.execute(it.toWeatherEntity())
                    .doOnError { Log.i("weather", it.localizedMessage) }
                    .andThen(Single.just(it))
            }
            .map { it.toWeatherModel() }
    }

    override fun getWeatherByCoordinates(coord: Coordinates): Single<BaseWeatherListModel> =
        iGetWeatherDataRepository.getByCoordinates(coord.lat, coord.lon)
            .flatMap {
                iSaveWeatherForCityUseCase.execute(it.toWeatherEntity().copy(isCurrentPlace = true))
                    .doOnError { Log.i("weather", it.localizedMessage) }
                    .andThen(Single.just(it))
            }
            .flatMap {
                iGetLocalWeatherDataRepository.updateCurrentFlag(it.id)
                    .andThen(Single.just(it))
            }
            .map { it.toWeatherModel() }

}