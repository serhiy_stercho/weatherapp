package com.seruslab.weatherapp.domain.useCases

import com.seruslab.weatherapp.data.response.weather.Coordinates
import com.seruslab.weatherapp.domain.model.BaseWeatherListModel
import io.reactivex.Single

interface IGetWeatherDataFromNetworkUseCase {
    fun getWeatherByName(name: String) : Single<BaseWeatherListModel>
    fun getWeatherByCoordinates(coord: Coordinates) : Single<BaseWeatherListModel>
}