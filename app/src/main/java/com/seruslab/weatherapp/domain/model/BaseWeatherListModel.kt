package com.seruslab.weatherapp.domain.model

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.seruslab.weatherapp.presentation.base.glide.GlideApp
import com.seruslab.weatherapp.presentation.fragments.home.adapter.model.CurrentCityWeatherModel
import com.seruslab.weatherapp.presentation.fragments.home.adapter.model.OtherCityWeatherModel

open class BaseWeatherListModel(
    open var cityId: Int?,
    open val cityName: String,
    open val humidity: Int,
    open val maxTemperature: Double,
    open val minTemperature: Double,
    open val main: String,
    open val icon: String,
    open val isCurrentPlace: Boolean
) {
    constructor() : this(0,"",0,0.toDouble(), 0.toDouble(),"", "", false)

    fun isEmpty() = cityName.isEmpty() && main.isEmpty() && icon.isEmpty()


    companion object {
        fun empty() = BaseWeatherListModel()

    }
}

fun BaseWeatherListModel.toCurrentCityWeatherModel() = CurrentCityWeatherModel(cityId,
    String.format("%s, %s", cityName, main),
    humidity, maxTemperature, minTemperature, main, icon, isCurrentPlace)

fun BaseWeatherListModel.toOtherCityWeatherModel() = OtherCityWeatherModel(cityId, cityName, humidity, maxTemperature, minTemperature, main, icon, false)