package com.seruslab.weatherapp.domain.useCases

import com.seruslab.weatherapp.domain.model.BaseWeatherListModel
import io.reactivex.Maybe
import io.reactivex.Single

interface IGetLocalWeatherDataUseCase {
    fun getWeatherByCityName(name: String) : Maybe<BaseWeatherListModel?>
    fun getAllCitiesWeather() : Single<List<BaseWeatherListModel>>
}