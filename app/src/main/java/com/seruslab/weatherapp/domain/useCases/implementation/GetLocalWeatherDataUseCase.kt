package com.seruslab.weatherapp.domain.useCases.implementation

import com.seruslab.weatherapp.data.persistence.entities.WeatherEntity
import com.seruslab.weatherapp.data.persistence.entities.toWeatherModel
import com.seruslab.weatherapp.data.repositories.IGetLocalWeatherDataRepository
import com.seruslab.weatherapp.domain.model.BaseWeatherListModel
import com.seruslab.weatherapp.domain.useCases.IGetLocalWeatherDataUseCase
import io.reactivex.Maybe
import io.reactivex.Single


class GetLocalWeatherDataUseCase(private val iGetLocalWeatherDataRepository: IGetLocalWeatherDataRepository) : IGetLocalWeatherDataUseCase {

    override fun getWeatherByCityName(name: String): Maybe<BaseWeatherListModel?> =
        iGetLocalWeatherDataRepository.getWeatherByCityName(name)
            .map {
                if (it.data == null)
                    return@map BaseWeatherListModel.empty()
                else return@map it.data.toWeatherModel()
            }

    override fun getAllCitiesWeather(): Single<List<BaseWeatherListModel>> =
        iGetLocalWeatherDataRepository.getAllCitiesWeather()
            .map {_weatherEntities ->
                _weatherEntities.map { it.toWeatherModel() }
            }

}