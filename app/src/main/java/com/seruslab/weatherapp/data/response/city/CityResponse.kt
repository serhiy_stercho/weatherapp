package com.seruslab.weatherapp.data.response.city

import com.google.gson.annotations.SerializedName

data class CityResponse(
    val description: String,
    @SerializedName("place_id")
    val placeId: String,
    @SerializedName("structured_formatting")
    val structuredFormatting: StructuredFormatting

) {
    data class StructuredFormatting(
        @SerializedName("main_text")
        val main: String
    )
}