package com.seruslab.weatherapp.data.api.applicationApi.client

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.seruslab.weatherapp.BuildConfig
import com.seruslab.weatherapp.data.api.base.BaseClient
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit

abstract class AppClient : BaseClient() {

    protected fun getAppRetrofitBuilder() : Retrofit.Builder {
        return getRetrofitBuilder().baseUrl(BuildConfig.BASE_URL)
    }

    protected fun getAppOkHttpClient() = getOkHttpClientBuilder()
        .addNetworkInterceptor(getQueryParameterInterceptor())
        .build()

    abstract fun getQueryParameterInterceptor() : Interceptor
}