package com.seruslab.weatherapp.data.response.weather

import com.google.gson.annotations.SerializedName
import com.seruslab.weatherapp.data.persistence.entities.WeatherEntity
import com.seruslab.weatherapp.domain.model.BaseWeatherListModel

data class WeatherResponse(
    val id: Int,
    val name: String,
    @SerializedName("coord")
    val coordinates: Coordinates,
    @SerializedName("main")
    val mainInfo: MainInfo,
    val weather: List<Weather>
)

fun WeatherResponse.toWeatherEntity() = WeatherEntity(
    cityId = id,
    cityName = name,
    coordonates = coordinates,
    humidity = mainInfo.humidity,
    maxTemperature = mainInfo.maxTemperature,
    minTemperature = mainInfo.minTemperature,
    main = weather[0].main,
    icon = weather[0].icon
)

fun WeatherResponse.toWeatherModel() = BaseWeatherListModel(
    cityId = id,
    cityName = name,
    //coordonates = coordinates,
    humidity = mainInfo.humidity,
    maxTemperature = mainInfo.maxTemperature,
    minTemperature = mainInfo.minTemperature,
    main = weather[0].main,
    icon = weather[0].icon,
    isCurrentPlace = false
)