package com.seruslab.weatherapp.data.persistence.daos

import androidx.room.Dao
import androidx.room.Query
import com.seruslab.weatherapp.data.persistence.base.BaseDao
import com.seruslab.weatherapp.data.persistence.entities.WeatherEntity
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface WeatherConditionsDao : BaseDao<WeatherEntity>{

    @Query("SELECT * FROM weather WHERE city_name in (:name)")
    fun selectWeatherByCityName(name: String) : Maybe<List<WeatherEntity>>

    @Query("SELECT * FROM weather WHERE city_id=:id")
    fun selectWeatherByCityId(id: Int) : Maybe<List<WeatherEntity>>

    @Query("SELECT * FROM weather")
    fun getAllCitiesWeather() : Single<List<WeatherEntity>>

    @Query("UPDATE weather SET is_current_place = 0 WHERE is_current_place = 1 AND city_id != :exceptId")
    fun clearCurrentFlag(exceptId: Int) : Completable

}