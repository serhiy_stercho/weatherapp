package com.seruslab.weatherapp.data.repositories

import com.seruslab.weatherapp.data.persistence.entities.WeatherEntity
import io.reactivex.Completable

interface ISaveWeatherDataLocallyRepository {
    fun saveWeatherConditions(weatherEntity: WeatherEntity) : Completable
}