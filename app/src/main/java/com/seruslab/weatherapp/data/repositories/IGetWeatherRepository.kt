package com.seruslab.weatherapp.data.repositories

import com.seruslab.weatherapp.data.response.weather.WeatherResponse
import io.reactivex.Single

interface IGetWeatherRepository {

    fun getByCityId(id: Int) : Single<WeatherResponse>
    fun getByCityName(name: String) : Single<WeatherResponse>
    fun getByCoordinates(lat: Double, lon: Double) : Single<WeatherResponse>
}