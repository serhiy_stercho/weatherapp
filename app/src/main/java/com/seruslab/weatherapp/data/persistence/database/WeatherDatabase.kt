package com.seruslab.weatherapp.data.persistence.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.seruslab.weatherapp.data.persistence.daos.WeatherConditionsDao
import com.seruslab.weatherapp.data.persistence.entities.WeatherEntity

@Database(
    entities = [
        WeatherEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class WeatherDatabase : RoomDatabase() {
    abstract fun getWeatherConditionsDao(): WeatherConditionsDao
}