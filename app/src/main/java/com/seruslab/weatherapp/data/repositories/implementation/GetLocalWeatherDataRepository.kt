package com.seruslab.weatherapp.data.repositories.implementation

import com.seruslab.weatherapp.data.persistence.entities.WeatherEntity
import com.seruslab.weatherapp.data.repositories.IGetLocalWeatherDataRepository
import com.seruslab.weatherapp.domain.model.BaseWeatherListModel
import com.seruslab.weatherapp.domain.model.base.BaseDomainModel
import com.seruslab.weatherapp.presentation.application.WeatherApplication
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class GetLocalWeatherDataRepository : IGetLocalWeatherDataRepository {

    override fun getWeatherByCityName(name: String): Maybe<BaseDomainModel<WeatherEntity>> =
        WeatherApplication.getInstance().weatherDatabase.getWeatherConditionsDao()
            .selectWeatherByCityName(name)
            .map {weathers -> BaseDomainModel(weathers.firstOrNull())}
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())

    override fun getAllCitiesWeather(): Single<List<WeatherEntity>> =
        WeatherApplication.getInstance().weatherDatabase.getWeatherConditionsDao()
            .getAllCitiesWeather()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())

    override fun updateCurrentFlag(exceptId: Int): Completable =
        WeatherApplication.getInstance().weatherDatabase.getWeatherConditionsDao()
            .clearCurrentFlag(exceptId)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
}