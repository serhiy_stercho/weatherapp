package com.seruslab.weatherapp.data.response.weather

import com.google.gson.annotations.SerializedName

data class Weather(
    val main: String,
    val description: String,
    val icon: String
)