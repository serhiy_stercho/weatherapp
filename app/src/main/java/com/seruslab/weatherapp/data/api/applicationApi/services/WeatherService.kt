package com.seruslab.weatherapp.data.api.applicationApi.services

import com.seruslab.weatherapp.data.response.weather.WeatherResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {
    @GET("weather")
    fun getWeatherByCityName(@Query("q") cityName: String) : Single<WeatherResponse>
    @GET("weather")
    fun getWeatherByPlaceId(@Query("id") cityId: Int) : Single<WeatherResponse>
    @GET("weather")
    fun getWeatherByCoordinates(@Query("lat") lat: Double, @Query("lon") lon: Double) : Single<WeatherResponse>
}