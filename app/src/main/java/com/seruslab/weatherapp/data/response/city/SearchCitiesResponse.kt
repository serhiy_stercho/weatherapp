package com.seruslab.weatherapp.data.response.city

import com.google.gson.annotations.SerializedName

data class SearchCitiesResponse(
    @SerializedName("predictions")
    val results: List<CityResponse>
)