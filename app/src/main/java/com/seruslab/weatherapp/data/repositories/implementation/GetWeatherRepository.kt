package com.seruslab.weatherapp.data.repositories.implementation

import com.seruslab.weatherapp.data.repositories.IGetWeatherRepository
import com.seruslab.weatherapp.data.response.weather.WeatherResponse
import com.seruslab.weatherapp.presentation.application.WeatherApplication
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class GetWeatherRepository : IGetWeatherRepository {

    override fun getByCityId(id: Int): Single<WeatherResponse> = WeatherApplication.getInstance()
        .weatherRestClient
        .weatherService
        .getWeatherByPlaceId(id)
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())

    override fun getByCityName(name: String): Single<WeatherResponse> = WeatherApplication.getInstance()
        .weatherRestClient
        .weatherService
        .getWeatherByCityName(name)
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())

    override fun getByCoordinates(lat: Double, lon: Double): Single<WeatherResponse> = WeatherApplication.getInstance()
        .weatherRestClient
        .weatherService
        .getWeatherByCoordinates(lat,lon)
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())
}