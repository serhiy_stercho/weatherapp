package com.seruslab.weatherapp.data.api.locationServicesApi.client

import com.seruslab.weatherapp.data.api.applicationApi.client.QueryParameterInterceptor
import com.seruslab.weatherapp.data.api.applicationApi.services.WeatherService
import com.seruslab.weatherapp.data.api.locationServicesApi.services.LocationService
import okhttp3.Interceptor
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.core.qualifier.named

class LocationServicesRestClient() : LocationClient(), KoinComponent {

    val parameterInterceptor: Interceptor by inject(named("locationQueryInterceptor"))

    lateinit var locationService: LocationService
        private set

    init {
        setupRestClient()
    }

    private fun setupRestClient() {
        val retrofit = getLocationsRetrofitBuilder()
            .client(getLocationsOkHttpClient())
            .build()

        locationService = retrofit.create(LocationService::class.java)
    }

    override fun getQueryParameterInterceptor(): Interceptor = parameterInterceptor
}