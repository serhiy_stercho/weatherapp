package com.seruslab.weatherapp.data.api.locationServicesApi.client

import com.seruslab.weatherapp.BuildConfig
import com.seruslab.weatherapp.data.api.base.BaseClient
import okhttp3.Interceptor
import retrofit2.Retrofit

abstract class LocationClient : BaseClient() {

    protected fun getLocationsRetrofitBuilder() : Retrofit.Builder {
        return getRetrofitBuilder().baseUrl(BuildConfig.PLACES_BASE_URL)
    }

    protected fun getLocationsOkHttpClient() = getOkHttpClientBuilder()
        .addNetworkInterceptor(getQueryParameterInterceptor())
        .build()

    abstract fun getQueryParameterInterceptor() : Interceptor
}