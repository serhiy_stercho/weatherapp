package com.seruslab.weatherapp.data.repositories.implementation

import com.seruslab.weatherapp.data.persistence.entities.WeatherEntity
import com.seruslab.weatherapp.data.repositories.ISaveWeatherDataLocallyRepository
import com.seruslab.weatherapp.presentation.application.WeatherApplication
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers

class SaveWeatherDataLocallyRepository : ISaveWeatherDataLocallyRepository {

    override fun saveWeatherConditions(weatherEntity: WeatherEntity): Completable =
        WeatherApplication.getInstance().weatherDatabase.getWeatherConditionsDao().insert(weatherEntity)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
}