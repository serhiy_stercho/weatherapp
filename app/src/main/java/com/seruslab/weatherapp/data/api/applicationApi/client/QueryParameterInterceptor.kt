package com.seruslab.weatherapp.data.api.applicationApi.client

import com.seruslab.weatherapp.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class QueryParameterInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val originalUrl = request.url

        val url = originalUrl.newBuilder()
            .addQueryParameter("units", "metric")
            .addQueryParameter("appid", BuildConfig.API_KEY)
            .build()

        val newRequest = request.newBuilder()
            .url(url)
            .build()

        return chain.proceed(newRequest)
    }
}