package com.seruslab.weatherapp.data.repositories.implementation

import com.seruslab.weatherapp.data.repositories.ISearchCitiesRepository
import com.seruslab.weatherapp.data.response.city.CityResponse
import com.seruslab.weatherapp.presentation.application.WeatherApplication
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class SearchCitiesRepository : ISearchCitiesRepository {

    override fun searchCityByQuery(query: String): Observable<List<CityResponse>> =
        WeatherApplication.getInstance().locationServicesRestClient.locationService
            .findCityByQuery(query)
            .map {response ->  response.results}
            .onErrorReturn {throwable ->  emptyList()}
            .subscribeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
}