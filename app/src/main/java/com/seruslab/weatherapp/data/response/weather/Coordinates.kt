package com.seruslab.weatherapp.data.response.weather

import com.google.gson.annotations.SerializedName

data class Coordinates(
    val lat: Double,
    val lon: Double
)