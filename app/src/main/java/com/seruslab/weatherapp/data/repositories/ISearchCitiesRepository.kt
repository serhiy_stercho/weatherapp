package com.seruslab.weatherapp.data.repositories

import com.seruslab.weatherapp.data.response.city.CityResponse
import io.reactivex.Observable


interface ISearchCitiesRepository {
    fun searchCityByQuery(query: String) : Observable<List<CityResponse>>
}