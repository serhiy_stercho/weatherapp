package com.seruslab.weatherapp.data.api.locationServicesApi.client

import com.seruslab.weatherapp.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class LocationQueryParameterInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val originalUrl = request.url

        val url = originalUrl.newBuilder()
            .addQueryParameter("key", BuildConfig.PLACES_API_KEY)
            .addQueryParameter("types", "(cities)")
            .build()

        val newRequest = request.newBuilder()
            .url(url)
            .build()

        return chain.proceed(newRequest)
    }
}