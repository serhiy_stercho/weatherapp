package com.seruslab.weatherapp.data.persistence.entities

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.seruslab.weatherapp.data.response.weather.Coordinates
import com.seruslab.weatherapp.data.response.weather.WeatherResponse
import com.seruslab.weatherapp.domain.model.BaseWeatherListModel

@Entity(tableName = "weather")
data class WeatherEntity(
    //@PrimaryKey(autoGenerate = true)
    val id: Long? = null,
    @PrimaryKey
    @ColumnInfo(name = "city_id")
    val cityId: Int?,
    @ColumnInfo(name = "city_name")
    val cityName: String?,
    @Embedded
    val coordonates: Coordinates,
    val humidity: Int,
    @ColumnInfo(name = "min_temp")
    val maxTemperature: Double,
    @ColumnInfo(name = "max_temp")
    val minTemperature: Double,
    val main: String,
    val icon: String,
    @ColumnInfo(name = "is_current_place")
    val isCurrentPlace: Boolean = false
) {


}

fun WeatherEntity.toWeatherModel() = BaseWeatherListModel(
    cityId = id?.toInt(),
    cityName = cityName ?: "",
    //coordonates = coordinates,
    humidity = humidity,
    maxTemperature = maxTemperature,
    minTemperature = minTemperature,
    main = main,
    icon = icon,
    isCurrentPlace = isCurrentPlace
)