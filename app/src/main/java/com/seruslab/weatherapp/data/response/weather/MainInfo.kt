package com.seruslab.weatherapp.data.response.weather

import com.google.gson.annotations.SerializedName

data class MainInfo(
    @SerializedName("temp")
    val temperature: Double,
    @SerializedName("feels_like")
    val feelsLike: Double,
    @SerializedName("temp_min")
    val minTemperature: Double,
    @SerializedName("temp_max")
    val maxTemperature: Double,
    val preasure: Int,
    val humidity: Int
)