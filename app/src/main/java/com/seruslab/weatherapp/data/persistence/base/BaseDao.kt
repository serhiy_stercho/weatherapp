package com.seruslab.weatherapp.data.persistence.base

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update
import io.reactivex.Completable

typealias ReturnResult = Completable

interface BaseDao<in T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg entities: T) : ReturnResult
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entities: List<T>) : ReturnResult
    @Update
    fun update(vararg entities: T) : ReturnResult
    @Update
    fun update(entities: List<T>) : ReturnResult
    @Delete
    fun delete(vararg entities: T) : ReturnResult
}