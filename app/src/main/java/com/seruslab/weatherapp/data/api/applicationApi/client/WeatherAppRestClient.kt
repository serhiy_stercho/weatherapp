package com.seruslab.weatherapp.data.api.applicationApi.client

import com.google.gson.annotations.SerializedName
import com.seruslab.weatherapp.data.api.applicationApi.services.WeatherService
import com.seruslab.weatherapp.data.api.base.BaseClient
import okhttp3.Interceptor
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.core.qualifier.named

class WeatherAppRestClient() : AppClient(), KoinComponent {

    val parameterInterceptor: Interceptor by inject(named("appQueryInterceptor"))

    lateinit var weatherService: WeatherService
        private set

    init {
        setupRestClient()
    }

    private fun setupRestClient() {
        val retrofit = getAppRetrofitBuilder()
            .client(getAppOkHttpClient())
            .build()

        weatherService = retrofit.create(WeatherService::class.java)
    }

    override fun getQueryParameterInterceptor(): Interceptor = parameterInterceptor
}