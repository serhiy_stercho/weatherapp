package com.seruslab.weatherapp.data.repositories

import com.seruslab.weatherapp.data.persistence.entities.WeatherEntity
import com.seruslab.weatherapp.domain.model.BaseWeatherListModel
import com.seruslab.weatherapp.domain.model.base.BaseDomainModel
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

interface IGetLocalWeatherDataRepository {
    fun getWeatherByCityName(name: String) : Maybe<BaseDomainModel<WeatherEntity>>
    fun getAllCitiesWeather() : Single<List<WeatherEntity>>
    fun updateCurrentFlag(exceptId: Int) : Completable
}