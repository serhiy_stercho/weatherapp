package com.seruslab.weatherapp.data.api.locationServicesApi.services

import com.seruslab.weatherapp.data.response.city.SearchCitiesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface LocationService {
    @GET("findplacefromtext/json")
    fun getPlacesByTextQuery(@Query("input") query: String) : Observable<Any>

    @GET("autocomplete/json")
    fun findCityByQuery(@Query("input") query: String) : Observable<SearchCitiesResponse>
}